/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.pojo;

/**
 * @author atnoce.com
 * @date 2016/11/17
 * @since 1.0.0
 */
public class DirTypeSync {
    private String dirId;
    private String dirName;
    private String createTime;
    private String modifyTime;
    private String isDelete;
    private String sync;

    public DirTypeSync(DirType dirType){
        this.dirId=dirType.getDirId();
        this.dirName=dirType.getDirName();
        this.createTime=dirType.getCreateTime();
        this.modifyTime=dirType.getModifyTime();
        this.sync=dirType.getSync();
    }
    public DirTypeSync(String dirId,String dirName,String createTime,String modifyTime,String sync,String isDelete){
        this.dirId=dirId;
        this.dirName=dirName;
        this.createTime=createTime;
        this.modifyTime=modifyTime;
        this.sync=sync;
        this.isDelete=isDelete;
    }
    public DirTypeSync(){}
    public String getDirId() {
        return dirId;
    }

    public void setDirId(String dirId) {
        this.dirId = dirId;
    }

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }
}
