/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.pojo;

import com.atnoce.onepass.utils.CommonUtil;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author atnoce.com
 * @date 2016/11/4
 * @since 1.0.0
 */
public class DirType {
    private final StringProperty dirId;
    private final StringProperty dirName;
    private final StringProperty createTime;
    private final StringProperty modifyTime;
    private final StringProperty isDelete;
    private final StringProperty sync;

    public DirType(){
        dirId=new SimpleStringProperty();
        dirName=new SimpleStringProperty();
        createTime=new SimpleStringProperty();
        modifyTime=new SimpleStringProperty();
        isDelete=new SimpleStringProperty();
        sync=new SimpleStringProperty();
    }
    public DirType(String dirName){
        this.dirId=new SimpleStringProperty(CommonUtil.getId());
        this.dirName=new SimpleStringProperty(dirName);
        this.createTime=new SimpleStringProperty(CommonUtil.getTime());
        this.modifyTime=new SimpleStringProperty(CommonUtil.getTime());
        this.sync=new SimpleStringProperty("false");
        this.isDelete=new SimpleStringProperty("false");
    }
    public DirType(DirType dirType){
        this.dirId=new SimpleStringProperty(dirType.getDirId());
        this.dirName=new SimpleStringProperty(dirType.getDirName());
        this.createTime=new SimpleStringProperty(dirType.getCreateTime());
        this.modifyTime=new SimpleStringProperty(dirType.getModifyTime());
        this.sync=new SimpleStringProperty(dirType.getSync());
        this.isDelete=new SimpleStringProperty(dirType.getIsDelete());
    }
    public DirType(String dirId,String dirName,String createTime,String modifyTime,String sync,String isDelete){
        this.dirId=new SimpleStringProperty(dirId);
        this.dirName=new SimpleStringProperty(dirName);
        this.createTime=new SimpleStringProperty(createTime);
        this.modifyTime=new SimpleStringProperty(modifyTime);
        this.sync=new SimpleStringProperty(sync);
        this.isDelete=new SimpleStringProperty(isDelete);
    }
    public String getDirId(){
        return dirId.get();
    }
    public void setDirId(String dirId){
        this.dirId.set(dirId);
    }
    public StringProperty dirIdProperty(){
        return dirId;
    }
    public String getDirName(){
        return dirName.get();
    }
    public void setDirName(String dirName){
        this.dirName.set(dirName);
    }
    public StringProperty dirNameProperty(){
        return dirName;
    }
    public String getCreateTime(){
        return createTime.get();
    }
    public void setCreateTime(String createTime){
        this.createTime.set(createTime);
    }
    public StringProperty createTimeProperty(){
        return createTime;
    }
    public String getSync(){
        return sync.get();
    }
    public void setSync(String sync){
        this.syncProperty().set(sync);
    }
    public StringProperty syncProperty(){
        return sync;
    }
    public String getModifyTime(){
        return modifyTime.get();
    }
    public void setModifyTime(String modifyTime){
        this.modifyTime.set(modifyTime);
    }
    public StringProperty modifyTimeProperty(){
        return modifyTime;
    }
    public String getIsDelete(){
        return isDelete.get();
    }
    public void setIsDelete(String isDelete){
        this.isDelete.set(isDelete);
    }
    public StringProperty isDeleteProperty(){
        return isDelete;
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return dirName.get();
    }
}
