/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.pojo;

/** 密码项的同步实体
 * @author atnoce.com
 * @date 2016/11/17
 * @since 1.0.0
 */
public class PasswordItemSync {
    private String passId;
    private String account;
    private String passwordStr;
    private String remarkStr;
    private String createTime;
    private String modifyTime;
    private String isDelete;
    private String dirTypeId;
    private String sync;

    public PasswordItemSync(){}
    public PasswordItemSync(PasswordItem passwordItem){
        passId=passwordItem.getPassId();
        account=passwordItem.getAccount();
        passwordStr=passwordItem.getPasswordStr();
        remarkStr=passwordItem.getRemarkStr();
        createTime=passwordItem.getCreateTime();
        modifyTime=passwordItem.getModifyTime();
        isDelete=passwordItem.getIsDelete();
        dirTypeId=passwordItem.getDirTypeId();
        sync=passwordItem.getSync();
    }
    public String getPassId() {
        return passId;
    }

    public void setPassId(String passId) {
        this.passId = passId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPasswordStr() {
        return passwordStr;
    }

    public void setPasswordStr(String passwordStr) {
        this.passwordStr = passwordStr;
    }

    public String getRemarkStr() {
        return remarkStr;
    }

    public void setRemarkStr(String remarkStr) {
        this.remarkStr = remarkStr;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getDirTypeId() {
        return dirTypeId;
    }

    public void setDirTypeId(String dirTypeId) {
        this.dirTypeId = dirTypeId;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
